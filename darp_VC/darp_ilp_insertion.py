from darp_ilp_common import *

#Not used

def objective(ss, x, ctx):
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    K = ctx.getK()
    n = ctx.getNbVertices()
    Arcs = forbiddenArcs(V, P, D, n)

    cost_time = ctx.getCost_time()
    cost_distance = ctx.getCost_distance()
    duration_stop = ctx.getDuration_stop()

    weights = ctx.getWeightsObj()
    emissions = ctx.getEmissionGHG()

    r = [0]*n
    for k in K:
        current_node = startDepot()
        current_time = 0
        while current_node != endDepot(n):
            bool_break = True
            for i in V:
                if (current_node, i) not in Arcs and bool_break:
                    if ss.get_var_value(x[current_node, i, k]) == 1:
                        current_time += duration_stop[current_node] + cost_time[current_node, i, 0]
                        if i in P:
                            r[i] = -1.0*(current_time+duration_stop[i])
                        if i in D:
                            r[pickup(i)] += current_time
                        current_node = i
                        bool_break = False

    cost_obj = weights[0]*sum(cost_time[i, j, 0]*ss.get_var_value(x[i, j, k])
                              for i in V for j in V if (i, j) not in Arcs for k in K) + \
               weights[1]*sum(r[i] for i in P)
    environ_obj = sum(emissions[0]*(cost_distance[i, j]/1000.0)*ss.get_var_value(x[i, j, k])
                      for i in V for j in V if (i, j) not in Arcs for k in K)

    return cost_obj + weights[2]*environ_obj


def checkConstraints_vehicle(node_old_prev, node_new, nodes_k, ctx):
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    Q = ctx.getCapacity()
    n = ctx.getNbVertices()
    request = ctx.getRequest()
    MAX_RIDING = ctx.getMaxRiding()
    cost_time = ctx.getCost_time()
    duration_stop = ctx.getDuration_stop()

    nodes_k_new = []
    for v in nodes_k:
        if v == node_old_prev:
            nodes_k_new.append(v)
            nodes_k_new.append(node_new)
        else:
            nodes_k_new.append(v)


    r = [0]*n
    current_cap = 0
    current_time = 0
    prev_v = startDepot()
    for idx_v, v in enumerate(nodes_k_new):
        if v != startDepot():
            current_time += duration_stop[prev_v] + cost_time[prev_v, v, 0]
        if v in P:
            r[v] = -1.0*(current_time + duration_stop[v])
        if v in D:
            r[pickup(v)] += current_time
            if r[pickup(v)] > MAX_RIDING:
                return False

        current_cap += request[v]
        if current_cap > Q:
            return False
    return True

def greedyInsertion(x, ss, nodes_ordered, model, ctx):
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    K = ctx.getK()
    n = ctx.getNbVertices()
    Arcs = forbiddenArcs(V, P, D, n)
    #print("ss", ss)

    ss_best = ss
    obj_best = objective(ss, x, ctx)
    print("greedyInsertion obj0 ", obj_best)
    ss_tmp = ss
    for k in K:
        nodes_k = nodes_ordered[k]
        print("nodes_k", nodes_k)
        for idx_p, p in enumerate(nodes_k):
            if p in P:
                print("remove node ", p)
                #remove
                prev_p = nodes_k[idx_p-1]
                ss_tmp.add_var_value(x[prev_p, p, k], 0)
                succ_p = nodes_k[idx_p+1]
                if succ_p == delivery(p):
                    succ_p = nodes_k[idx_p+2]
                    ss_tmp.add_var_value(x[p, delivery(p), k], 0)
                else:
                    ss_tmp.add_var_value(x[p, succ_p, k], 0)
                print("p", p,"prev_p", prev_p,"succ_p", succ_p)
                ss_tmp.add_var_value(x[prev_p, succ_p, k], 1)

                for idx_d, d in enumerate(nodes_k):
                    if d == delivery(p):
                        prev_d = nodes_k[idx_d-1]
                        if prev_d == p:
                            prev_d = nodes_k[idx_d-2]
                            ss_tmp.add_var_value(x[p, d, k], 0)
                        else:
                            ss_tmp.add_var_value(x[prev_d, d, k], 0)
                        succ_d = nodes_k[idx_d+1]
                        print("d", d,"prev_d", prev_d,"succ_d", succ_d)
                        ss_tmp.add_var_value(x[d, succ_d, k], 0)
                        ss_tmp.add_var_value(x[prev_d, succ_d, k], 1)


                        #insert
                        for k1 in K:
                            if k1 != k:
                                print("insert v", k1)
                                nodes_k1 = nodes_ordered[k1]
                                for idx_v, v in enumerate(nodes_k1):
                                    if v != endDepot(n): #end
                                        check = checkConstraints_vehicle(v, p, nodes_k1, ctx)
                                        print("v", v, "check",check)
                                        if check:
                                            succ_v = nodes_k1[idx_v+1]
                                            print("v", v,"p", p,"succ_v", succ_v)
                                            if succ_v != endDepot(n): #TODO no spqce to insert delivery(p)
                                                ss_tmp.add_var_value(x[v, p, k1], 1)
                                                ss_tmp.add_var_value(x[p, succ_v, k1], 1)
                                                ss_tmp.add_var_value(x[v, succ_v, k1], 0)

                                                for idx_w, w in enumerate(nodes_k1):
                                                    if idx_w >= idx_v and w != endDepot(n):
                                                        check = checkConstraints_vehicle(w, d, nodes_k1, ctx)
                                                        print("w", w, "check",check)
                                                        if check:
                                                            succ_w = nodes_k1[idx_w+1]
                                                            print("w", w,"delivery(p)", delivery(p),"succ_w", succ_w)
                                                            if v == w:
                                                                ss_tmp.add_var_value(x[p, delivery(p), k1], 1)
                                                            else :
                                                                ss_tmp.add_var_value(x[w, delivery(p), k1], 1)
                                                            ss_tmp.add_var_value(x[delivery(p), succ_w, k1], 1)
                                                            ss_tmp.add_var_value(x[w, succ_w, k1], 0)

                                                            print("coucou obj ")
                                                            obj_tmp = objective(ss_tmp, x, ctx)
                                                            if obj_tmp < obj_best:
                                                                ss_best = ss_tmp
                                                                print("greedyInsertion obj1 ", obj_tmp, ss_best)
                                                                return ss_best



    #return ss_best
