import matplotlib.pyplot as plt
import numpy as np

#Old plots

#
#1. sensitivity to nbV
#
nbV=range(2,13)
VKT_nbV=[43.729,42.297,45.036,43.166,47.092,43.909,44.262,53.572,58.418,51.222,59.936]
VUR_nbV=[47.0,51.8,47.0,49.1,45.9,50.1,50.9,47.0,45.6,50.1,42.9]
GHG_nbV=[7.478,7.233 ,7.701 ,7.381 ,8.053 ,7.508,7.569,9.161 ,9.989 ,8.759 ,10.249]

#Fig1
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, VKT_nbV, 'ro-', c='r', alpha=0.5)
plt.title('VKT in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([41,61])
plt.xlabel('vehicles number', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/nbV-VKT.png')

#Fig2
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, VUR_nbV, 'ro-', c='b', alpha=0.5)
plt.title('VOR in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([25,63])
plt.xlabel('vehicles number', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/nbV-VOR.png')


#Fig3
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, GHG_nbV, 'ro-', c='g', alpha=0.5)
plt.title('GHG in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([7,10.5])
plt.xlabel('vehicles number', fontsize=14)
plt.ylabel('GHG in Kg CO2-eq', fontsize=14)
#plt.show()
fig.savefig('run/nbV-GHG.png')


#
#2. sensitivity to maxR
#

maxR=[15, 20, 25, 30, 35, 40]
VKT_maxR=[43.166, 46.344, 45.036, 42.297, 43.165, 44.473]
VUR_maxR=[48, 47, 47, 51.8, 48,49]
GHG_maxR=[7.381 ,7.925 ,7.701  ,7.233,7.381 ,7.605]

#Fig4
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, VKT_maxR, 'ro-', c='r', alpha=0.5)
plt.title('VKT in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([41,61])
plt.xlabel('Maximum riding time in minutes', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/maxR-VKT.png')

#Fig5
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, VUR_maxR, 'ro-', c='b', alpha=0.5)
plt.title('VOR in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([25,63])
plt.xlabel('Maximum riding time in minutes', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/maxR-VOR.png')


#Fig6
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, GHG_maxR, 'ro-', c='g', alpha=0.5)
plt.title('GHG in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([7,10.5])
plt.xlabel('Maximum riding time in minutes', fontsize=14)
plt.ylabel('GHG in Kg CO2-eq', fontsize=14)
#plt.show()
fig.savefig('run/maxR-GHG.png')



#
#3. sensitivity to capacity
#

cap=[20, 25, 30, 35, 40, 45]
VKT_cap=[44.741, 45.036, 44.168, 42.965, 43.165, 43.729]
VUR_cap=[61.4, 47.0, 41.4, 35.5, 30.0, 26.1]
GHG_cap=[7.651, 7.701, 7.553, 7.347, 7.381 , 7.478]


#Fig7
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, VKT_cap, 'ro-', c='r', alpha=0.5)
plt.title('VKT in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([41,61])
plt.xlabel('Vehicle capacity', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/cap-VKT.png')

#Fig8
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, VUR_cap, 'ro-', c='b', alpha=0.5)
plt.title('VOR in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([25,63])
plt.xlabel('Vehicle capacity', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/cap-VOR.png')


#Fig9
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, GHG_cap, 'ro-', c='g', alpha=0.5)
plt.title('GHG in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_ylim([7,10.5])
plt.xlabel('Vehicle capacity', fontsize=14)
plt.ylabel('GHG in Kg CO2-eq', fontsize=14)
#plt.show()
fig.savefig('run/cap-GHG.png')
