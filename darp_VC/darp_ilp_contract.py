from darp_ilp_common import *

# 6 combinations of P1, D1, P2, D2 exist ...
def computeCosts(p1, p2, cost_time, request, cap, maxR):
    count = 0
    cos1 = cos2 = cos3 = cos4 = cos5 = cos6 = INFTY
    #P1->P2->D1->D2
    cost_p_delivery_p = cost_time[p1, p2,0] + cost_time[p2, delivery(p1),0] + cost_time[delivery(p1), delivery(p2),0]
    if request[p1] + request[p2] < cap and cost_p_delivery_p <= maxR:
        cos1 = cost_p_delivery_p
        count += 1
    #P1->P2->D2->D1
    cost_p_delivery_p = cost_time[p1, p2,0] + cost_time[p2, delivery(p2),0] + cost_time[delivery(p2), delivery(p1),0]
    if request[p1] + request[p2] < cap and cost_p_delivery_p <= maxR:
        cos2 = cost_p_delivery_p
        count += 1
    #P2->P1->D1->D2
    cost_p_delivery_p = cost_time[p2, p1,0] + cost_time[p1, delivery(p1),0] + cost_time[delivery(p1), delivery(p2),0]
    if request[p1] + request[p2] < cap and cost_p_delivery_p <= maxR:
        cos4 = cost_p_delivery_p
        count += 1
    #P2->P1->D2->D1
    cost_p_delivery_p = cost_time[p2, p1,0] + cost_time[p1, delivery(p2),0] + cost_time[delivery(p2), delivery(p1),0]
    if request[p1] + request[p2] < cap and cost_p_delivery_p <= maxR:
        cos5 = cost_p_delivery_p
        count += 1
    #P1->D1->P2->D2
    cost_p_delivery_p = cost_time[p1, delivery(p1),0] + cost_time[delivery(p1), p2,0] + cost_time[p2, delivery(p2),0]
    if cost_p_delivery_p <= maxR:
        cos3 = cost_p_delivery_p
        count += 1
    #P2->D2->P1->D1
    cost_p_delivery_p = cost_time[p2, delivery(p2),0] + cost_time[delivery(p2), p1,0] + cost_time[p1, delivery(p1),0]
    if cost_p_delivery_p <= maxR:
        cos6 = cost_p_delivery_p
        count += 1
    return cos1, cos2, cos3, cos4, cos5, cos6



def fillOrderPndD(p1, p2, cos, cos1, cos2, cos3, cos4, cos5, cos6, nodeToReplace,
                  embeddedOrder, cost_time, logging):
    cost_inside = 0
    #P1->P2->D1->D2
    if cos == cos1:
        embeddedOrder[p1] = (p1, p2, delivery(p1), delivery(p2))
        embeddedOrder[delivery(p1)] = (p1, p2, delivery(p1), delivery(p2))
        nodeToReplace[p1] = (p1, p2)
        nodeToReplace[delivery(p1)] = (delivery(p1), delivery(p2))
        nodeToReplace[p2] = nodeToReplace[delivery(p2)] = -1 #Toremove
        cost_inside = cost_time[p1, p2,0] + cost_time[delivery(p1), delivery(p2),0]
        if logging: print("P1->P2->D1->D2: ", p1, p2, delivery(p1), delivery(p2), " cost: ", cos)
    #P1->P2->D2->D1
    elif cos == cos2:
        embeddedOrder[p1] =  (p1, p2, delivery(p2), delivery(p1))
        embeddedOrder[delivery(p1)] = (p1, p2, delivery(p2), delivery(p1))
        nodeToReplace[p1] = (p1, p2)
        nodeToReplace[delivery(p1)] = (delivery(p2), delivery(p1))
        nodeToReplace[p2] = nodeToReplace[delivery(p2)] = -1 #Toremove
        cost_inside = cost_time[p1, p2,0] + cost_time[delivery(p2), delivery(p1),0]
        if logging: print("P1->P2->D2->D1: ", p1, p2, delivery(p2), delivery(p1), " cost: ", cos)
    #P1->D1->P2->D2
    elif cos == cos3:
        embeddedOrder[p1] = (p1, delivery(p1), p2, delivery(p2))
        embeddedOrder[delivery(p1)] = (p1, delivery(p1), p2, delivery(p2))
        nodeToReplace[p1] = (p1, delivery(p1))
        nodeToReplace[delivery(p1)] = (p2, delivery(p2))
        nodeToReplace[p2] = nodeToReplace[delivery(p2)] = -1 #Toremove
        cost_inside = cost_time[p1, delivery(p1),0] + cost_time[p2, delivery(p2),0]
        if logging: print("P1->D1->P2->D2: ", p1, delivery(p1), p2, delivery(p2), " cost: ", cos)
    #P2->P1->D1->D2
    elif cos == cos4:
        embeddedOrder[p2] = (p2, p1, delivery(p1), delivery(p2))
        embeddedOrder[delivery(p2)] = (p2, p1, delivery(p1), delivery(p2))
        nodeToReplace[p2] = (p2, p1)
        nodeToReplace[delivery(p2)] = (delivery(p1), delivery(p2))
        nodeToReplace[p1] = nodeToReplace[delivery(p1)] = -1 #Toremove
        cost_inside = cost_time[p2, p1,0] + cost_time[delivery(p1), delivery(p2),0]
        if logging: print("P2->P1->D1->D2: ", p2, p1, delivery(p1), delivery(p2), " cost: ", cos)
    elif cos == cos5: #P2->P1->D2->D1
        embeddedOrder[p2] = (p2, p1, delivery(p2),delivery(p1))
        embeddedOrder[delivery(p2)] = (p2, p1, delivery(p2),delivery(p1))
        nodeToReplace[p2] = (p2, p1)
        nodeToReplace[delivery(p2)] = (delivery(p2), delivery(p1))
        nodeToReplace[p1] = nodeToReplace[delivery(p1)] = -1
        cost_inside = cost_time[p2, p1,0] + cost_time[delivery(p2), delivery(p1),0]
        if logging: print("P2->P1->D2->D1: ", p2, p1, delivery(p2), delivery(p1), " cost: ", cos)
    elif cos == cos6: #P2->D2->P1->D1
        embeddedOrder[p2] = (p2, delivery(p2), p1, delivery(p1))
        embeddedOrder[delivery(p2)] = (p2, delivery(p2), p1, delivery(p1))
        nodeToReplace[p2] = (p2, delivery(p2))
        nodeToReplace[delivery(p2)] = (p1, delivery(p1))
        nodeToReplace[p1] = nodeToReplace[delivery(p1)] = -1
        cost_inside = cost_time[p2, delivery(p2),0] + cost_time[p1, delivery(p1),0]
        if logging: print("P2->D2->P1->D1: ", p2, delivery(p2), p1, delivery(p1), " cost: ", cos)
    return nodeToReplace, embeddedOrder, cost_inside



def orderCouplePandD(nbReduction, sorted_list, cost_time, request, maxR, cap, Vnames, logging):
    nodeToReplace = embeddedOrder = {}
    total_cost = total_cost_inside = 0.0
    count_reduced = count_iteration = 0
    times = 1000
    sorted_list_init = sorted_list

    #first try the initial order provided in sorted_list in case this order
    #does not provide a reduction remove two elments of the list each time
    # final strategy is to shuffle the list ..

    while count_reduced < nbReduction and count_iteration < times:
        nodeToReplace = {}
        embeddedOrder = {}
        total_cost = total_cost_inside = 0.0
        count_reduced = 0
        for i in range(len(sorted_list)):
            if count_reduced >= nbReduction: break
            p1 = int(sorted_list[i][0][0])
            p2 = int(sorted_list[i][0][1])

            if p1 not in nodeToReplace and p2 not in nodeToReplace:
                if logging: print((p1, p2) + (Vnames[p1], Vnames[p2]))
                cos1, cos2, cos3, cos4, cos5, cos6 = computeCosts(p1, p2, cost_time, request, cap, maxR)
                cos = min(cos1, cos2, cos4, cos3, cos5, cos6)
                if cos != INFTY and cos < maxR:
                    count_reduced += 2
                    nodeToReplace, embeddedOrder, cost_inside = \
                        fillOrderPndD(p1, p2, cos, cos1, cos2, cos3, cos4, cos5, cos6,
                                      nodeToReplace, embeddedOrder, cost_time, logging)
                    total_cost += cos
                    total_cost_inside += cost_inside
                    #print("coucou2 ", p1, p2, total_cost)
        #print("count_reduced", count_reduced, "nbReduction ", nbReduction, "len list", len(sorted_list))
        sorted_list = sorted_list_init[:count_iteration] + sorted_list_init[count_iteration+2 :]
        if count_iteration > (1-0.2)*times:
            sorted_list = sorted(sorted_list_init, key=lambda k: random.random())
        count_iteration += 1

    return nodeToReplace, embeddedOrder, total_cost, total_cost_inside, count_reduced



def setNewV(nodeToReplace, embeddedOrder, V, P, D, request, nbVertices):
    V_new=[]; P_new=[]; D_new=[]
    request_new = np.zeros(nbVertices, dtype=float)
    mappingVinit ={}
    #mappingVinit_inverse ={}
    count = 0
    for i in V:
        if i not in nodeToReplace: #add normally
            mappingVinit[count] = i
            #mappingVinit_inverse[i] =count
            V_new.append(count)
            if i in P: P_new.append(count)
            if i in D: D_new.append(count)
            request_new[count] = request[i]
            count += 1
        elif i in nodeToReplace and nodeToReplace[i] != -1: #add with the new mapping
            V_new.append(count)
            mappingVinit[count] = nodeToReplace[i]
            if i in P:
                P_new.append(count)
                request_new[count] = sum([request[p] for p in embeddedOrder[i] if request[p] >= 0.0])*0.5
            if i in D:
                D_new.append(count)
                request_new[count] = sum([request[p] for p in embeddedOrder[i] if request[p] <= 0.0])*0.5
            count += 1
    nbVertices_new = count
    return V_new, P_new, D_new, request_new, nbVertices_new, mappingVinit



def setNewCosts(cost_time, cost_distance, durationStop, mappingVinit, nbVertices_new):
    cost_time_new = np.zeros((nbVertices_new, nbVertices_new, 1), dtype=int)
    cost_distance_new = np.zeros((nbVertices_new, nbVertices_new), dtype=int)

    for idx_row in range(nbVertices_new):
        for idx_col in range(nbVertices_new):
            if idx_row != idx_col:
                prev_row = mappingVinit[idx_row]
                prev_col = mappingVinit[idx_col]
                if isinstance(prev_row, int) and isinstance(prev_col, int):
                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row, prev_col, 0]
                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row, prev_col]
                elif isinstance(prev_row, int) and not isinstance(prev_col, int):
                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row, prev_col[0], 0]
                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row, prev_col[0]]
                elif not isinstance(prev_row, int) and isinstance(prev_col, int):
                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[0], prev_row[1], 0] + \
                                                         cost_time[prev_row[1], prev_col, 0]
                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[0], prev_row[1]] + \
                                                          cost_distance[prev_row[1], prev_col]
                elif not isinstance(prev_row, int) and not isinstance(prev_col, int):
                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[0], prev_row[1], 0] + \
                                                         cost_time[prev_row[1], prev_col[0], 0] + \
                                                         cost_time[prev_col[0], prev_col[1], 0]
                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[0], prev_row[1]] + \
                                                          cost_distance[prev_row[1], prev_col[0]] + \
                                                          cost_distance[prev_col[0], prev_col[1]]
    #costs to infinity going to origin depot or coming from end depot
    for idx in range(nbVertices_new):
        cost_time_new[idx, 0, 0] = INFTY
        cost_distance_new[idx, 0] = INFTY
        cost_time_new[nbVertices_new-1, idx, 0] = INFTY
        cost_distance_new[nbVertices_new-1, idx] = INFTY
    #Create stop durations
    duration_stop_new = np.zeros(nbVertices_new, dtype=int)
    for idx in range(nbVertices_new): duration_stop_new[idx] = durationStop
    duration_stop_new[0] = duration_stop_new[nbVertices_new-1] = 0
    return cost_time_new, cost_distance_new, duration_stop_new


def setNewVname(V_new, Vname, maptoVinit):
    Vname_new=[]
    for i in V_new:
        v = maptoVinit[i]
        if isinstance(v, int):
            Vname_new.append(Vname[v])
        else:
            Vname_new.append((Vname[v[0]], Vname[v[1]]))
    return Vname_new

