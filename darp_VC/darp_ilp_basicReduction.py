from darp_ilp_common import *
from darp_ilp_contract import *

#
# Reduction based on shortest paths
#


def getCouplePandD_Basic(G_init, P, cost_time, request, cap, maxR):
    couplePandD = {}
    for i in P:
        for j in P:
            if i != j:
                measure = cost_time[i, j, 0] + cost_time[delivery(j), delivery(i), 0] + \
                          cost_time[i, delivery(i), 0] + cost_time[j, delivery(j), 0] + \
                          cost_time[i, delivery(j), 0] + cost_time[delivery(i), j, 0]
                measure += cost_time[j, i, 0] + cost_time[delivery(j), delivery(i), 0] + \
                           cost_time[delivery(i), i, 0] + cost_time[delivery(j), j, 0] + \
                           cost_time[delivery(j), i, 0] + cost_time[j, delivery(i), 0]
                measure = measure / 12.0
                #measure = cost_time[i, j, 0]

                #only consider couple that satidfy cap and maxR constraints
                if min(computeCosts(i, j, cost_time, request, cap, maxR)) != INFTY:
                    couplePandD[(i, j)] = measure
    return sorted(couplePandD.items(), key=operator.itemgetter(1), reverse=False)



def contractBasic(G_init, ctx_init, rContra, durationStop, cap, maxR, logging):
    if logging: print("\n\nBasic")
    V = ctx_init.getV()
    P = ctx_init.getP()
    D = ctx_init.getD()
    Vname = ctx_init.getVname()
    nbVertices = ctx_init.getNbVertices()
    request = ctx_init.getRequest()
    cost_time = ctx_init.getCost_time()
    cost_distance = ctx_init.getCost_distance()


    #1. Reduction
    nbReduction = int((nbVertices-2)*rContra)
    listPandD = getCouplePandD_Basic(G_init, P, cost_time, request, cap, maxR)
    nodeToReplace, embeddedOrder, total_cost, total_cost_inside, count_reduced = \
        orderCouplePandD(nbReduction, listPandD, cost_time, request, maxR, cap, Vname, logging)
    #print("cost reduction (B)", total_cost, "node reduced", count_reduced, nodeToReplace, "\n")

    #construct a new context for optimization
    V_new, P_new, D_new, request_new, nbVertices_new, maptoVinit = \
        setNewV(nodeToReplace, embeddedOrder, V, P, D, request, nbVertices)
    cost_time_new, cost_distance_new, duration_stop_new = \
        setNewCosts(cost_time, cost_distance, durationStop, maptoVinit, nbVertices_new)
    request_new = request_new[0:int(nbVertices_new)]
    Vname_new = setNewVname(V_new, ctx_init.getVname(), maptoVinit)
    #print("coco", len(maptoVinit), maptoVinit)

    return ContextOptim(V=V_new, P=P_new, D=D_new,
                        request=request_new,
                        nbVertices=nbVertices_new,
                        nbVehicles=ctx_init.getNbVehicles(),
                        nbTimeSteps=1,
                        capacity=cap,
                        max_riding= maxR,
                        horizon=ctx_init.getHorizon(),
                        cost_time=cost_time_new,
                        cost_distance=cost_distance_new,
                        duration_stop=duration_stop_new,
                        weightsObj=ctx_init.getWeightsObj(),
                        emissionGHG=ctx_init.getEmissionGHG(),
                        Vname=Vname_new), maptoVinit


