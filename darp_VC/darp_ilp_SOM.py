from darp_ilp_common import *
from darp_ilp_contract import *


#
# Reduction based on Single oriented maps, deep learning method
#

def getCouplePandD_SOM(G_init, P, cost_time,i_i,j_j,k_k):
    X = []
    vertexToCls={}
    penality = 100
    for i in P:
        measure = 0.0
        count = 0
        for j in P:
            if i != j:
                measure += cost_time[i, j, 0] + cost_time[delivery(i), delivery(j), 0] + \
                           cost_time[i, delivery(i), 0] + cost_time[j, delivery(j), 0] + \
                           cost_time[i, delivery(j), 0] + cost_time[delivery(i), j, 0]
                measure += cost_time[j, i, 0] + cost_time[delivery(j), delivery(i), 0] + \
                           cost_time[delivery(i), i, 0] + cost_time[delivery(j), j, 0] + \
                           cost_time[delivery(j), i, 0] + cost_time[j, delivery(i), 0]
                count += 1
        X.append((float(measure/(12.0*count)), 0))
        vertexToCls[i] = len(X)-1
        nbClusters=10;
    #nbClusters = int(len(X)/2)+1 # = nbPickup/2
    print("nb clusters",nbClusters)
    
    # Initialization and training
    #som_shape = (int(nbClusters**0.5),int(nbClusters**0.5))
    som_shape=(4,4)
    som = MiniSom(som_shape[0], som_shape[1], 2, sigma=i_i, learning_rate=j_j,
                  neighborhood_function='gaussian',activation_distance="chebyshev",random_seed=0,topology='rectangular')
    som.train_batch(X, k_k, verbose=True)
    
    # each neuron represents a cluster
    winner_coordinates = np.array([som.winner(x) for x in X]).T
    # with np.ravel_multi_index we convert the bidimensional
    # coordinates to a monodimensional index
    cluster_index = np.ravel_multi_index(winner_coordinates, som_shape)
    print("cluster_index",cluster_index)
    print("nb de clusters : ",len(np.unique(cluster_index)))
    
    #print("nbClusters ", nbClusters, "X", len(X), " ",X)
    #revoi une liste de nombre, meme nombre si meme cluster
    #clus2 = KMeansConstrained(n_clusters=nbClusters, size_min=0, size_max=2, init='random').fit(X).labels_
    #print("clus2",clus2)

    couplePandD = {}
    for i in P:
        for j in P:
            if i != j:
                if (i, j) not in couplePandD:
                    id_i = vertexToCls[i]
                    id_j = vertexToCls[j]
                    if cluster_index[id_i] == cluster_index[id_j]:
                        couplePandD[(i, j)] = (X[id_i][0]+X[id_j][0])*0.5
                        couplePandD[(j, i)] = (X[id_i][0]+X[id_j][0])*0.5
                    else:
                        #Not in cluster but added in case the cluster couplePandD do
                        # not satisfy the constraints ...
                        couplePandD[(i, j)] = (X[id_i][0]+X[id_j][0])*0.5 + penality
                        couplePandD[(j, i)] = (X[id_i][0]+X[id_j][0])*0.5 + penality
    print("couplepand",couplePandD.items())
    return sorted(couplePandD.items(), key=operator.itemgetter(1), reverse=False)






def contractSOM(G_init, ctx_init, rContra, durationStop, cap, maxR, times, logging,i_i,j_j,k_k):
    #print("\n\nSOM")
    V = ctx_init.getV()
    P = ctx_init.getP()
    D = ctx_init.getD()
    Vname = ctx_init.getVname()
    nbVertices = ctx_init.getNbVertices()
    request = ctx_init.getRequest()
    cost_time = ctx_init.getCost_time()
    cost_distance = ctx_init.getCost_distance()

    #1. Reduction
    #nbReduction = int((nbVertices*rContra)**0.5)*int((nbVertices*rContra)**0.5)
    nbReduction = int((nbVertices-2)*rContra)
    if nbReduction % 2 ==1: nbReduction = nbReduction -1

    total_cost_best = total_cost_inside_best = INFTY
    nodeToReplace_best = {}
    embeddedOrder_best = {}
    count_reduced_best = None
    for t in range(times):
        listPandD = getCouplePandD_SOM(G_init, P, cost_time,i_i,j_j,k_k)
        #print("coucou ", len(listPandD), listPandD)
        nodeToReplace, embeddedOrder, total_cost, total_cost_inside, count_reduced = \
            orderCouplePandD(nbReduction, listPandD, cost_time, request, maxR, cap, Vname, logging)
        #print("cost reduction (clus)", total_cost, "node reduced", count_reduced, nodeToReplace, "\n")
        #print("coucou", count_reduced, nbReduction)
        if total_cost < total_cost_best and count_reduced == nbReduction:
            nodeToReplace_best = nodeToReplace
            embeddedOrder_best = embeddedOrder
            total_cost_best = total_cost
            total_cost_inside_best = total_cost_inside
            count_reduced_best = count_reduced
    #print("cost reduction (Clus)", total_cost_best, "node reduced", count_reduced_best, nodeToReplace_best, "\n")



    #construct a new context for optimization
    V_new, P_new, D_new, request_new, nbVertices_new, maptoVinit = \
        setNewV(nodeToReplace_best, embeddedOrder_best, V, P, D, request, nbVertices)
    cost_time_new, cost_distance_new, duration_stop_new = \
        setNewCosts(cost_time, cost_distance, durationStop, maptoVinit, nbVertices_new)
    request_new = request_new[0:int(nbVertices_new)]
    Vname_new = setNewVname(V_new, ctx_init.getVname(), maptoVinit)
    #print("coco", len(maptoVinit), maptoVinit)

    return ContextOptim(V=V_new, P=P_new, D=D_new,
                        request=request_new,
                        nbVertices=nbVertices_new,
                        nbVehicles=ctx_init.getNbVehicles(),
                        nbTimeSteps=1,
                        capacity=cap,
                        max_riding= maxR,
                        horizon=ctx_init.getHorizon(),
                        cost_time=cost_time_new,
                        cost_distance=cost_distance_new,
                        duration_stop=duration_stop_new,
                        weightsObj=ctx_init.getWeightsObj(),
                        emissionGHG=ctx_init.getEmissionGHG(),
                        Vname=Vname_new), maptoVinit


